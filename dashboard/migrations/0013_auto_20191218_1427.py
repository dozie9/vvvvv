# Generated by Django 2.1.7 on 2019-12-18 14:27

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0012_auto_20190816_1325'),
    ]

    operations = [
        migrations.CreateModel(
            name='Announcements',
            fields=[
                ('ann_id', models.AutoField(primary_key=True, serialize=False)),
                ('subject', models.CharField(default='', max_length=300)),
                ('mail_body', models.CharField(default='', max_length=600)),
                ('dashboard_msg', models.CharField(default='', max_length=600)),
                ('created_on', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 935620))),
                ('updated_on', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 935650))),
                ('status', models.CharField(default='deactive', max_length=30)),
            ],
            options={
                'db_table': 'announcements',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AnnouncementsAccess',
            fields=[
                ('table_id', models.AutoField(primary_key=True, serialize=False)),
                ('announcement_id', models.CharField(default='', max_length=300)),
                ('user_id', models.CharField(default='', max_length=600)),
                ('timestamp', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 936132))),
            ],
            options={
                'db_table': 'announcements_access',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='CampaignStatus',
            fields=[
                ('status_id', models.AutoField(primary_key=True, serialize=False)),
                ('campaign_id', models.IntegerField()),
                ('contact', models.CharField(max_length=50)),
                ('contact_type', models.CharField(max_length=20)),
                ('created_on', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 939067))),
                ('updated_on', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 939089))),
                ('status', models.CharField(default='scheduled', max_length=30)),
            ],
            options={
                'db_table': 'campaign_status',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='CampaignTemplates',
            fields=[
                ('template_id', models.AutoField(primary_key=True, serialize=False)),
                ('event_id', models.IntegerField()),
                ('user_id', models.IntegerField()),
                ('predefined_template_id', models.IntegerField(default=0)),
                ('template_type', models.CharField(max_length=50)),
                ('template_subject', models.CharField(default='', max_length=100)),
                ('template_image', models.CharField(default='', max_length=300)),
                ('template_msg', models.CharField(default='', max_length=160)),
                ('template_link', models.CharField(default='', max_length=4000)),
                ('is_standard', models.IntegerField(default=0)),
                ('status', models.CharField(default='active', max_length=30)),
                ('created_on', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 938538))),
                ('scheduled_on', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 938563))),
            ],
            options={
                'db_table': 'campaign_templates',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='CategoriesStalls',
            fields=[
                ('table_id', models.AutoField(primary_key=True, serialize=False)),
                ('category', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'categories_stalls',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ErcessOffers',
            fields=[
                ('table_id', models.AutoField(primary_key=True, serialize=False)),
                ('admin_id', models.IntegerField()),
                ('date_added', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 937445))),
                ('country', models.IntegerField()),
                ('coupon_name', models.CharField(max_length=20)),
                ('price_range_min', models.IntegerField()),
                ('price_range_max', models.IntegerField()),
                ('cashback', models.IntegerField()),
                ('discount_amt', models.IntegerField()),
                ('status', models.CharField(default='active', max_length=30)),
            ],
            options={
                'db_table': 'ercess_offers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Leads',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=50)),
                ('email', models.CharField(default='', max_length=60)),
                ('contact', models.CharField(default='', max_length=12)),
                ('city', models.CharField(default='', max_length=30)),
                ('category', models.IntegerField(default='')),
                ('sub_category', models.IntegerField(default='')),
                ('user_id', models.IntegerField(default='')),
                ('date_added', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 934391))),
            ],
            options={
                'db_table': 'leads',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PackageSales',
            fields=[
                ('table_id', models.AutoField(primary_key=True, serialize=False)),
                ('user_id', models.IntegerField(default=0)),
                ('purchase_date', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 934925))),
                ('booking_id', models.CharField(default='', max_length=50)),
                ('price_paid', models.CharField(default='', max_length=11)),
                ('package_bought', models.IntegerField(default=0)),
                ('invoice_number', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'package_sales',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ReferrerCashbackInfo',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('number', models.CharField(default='', max_length=20)),
                ('payment_platform', models.CharField(default='', max_length=50)),
                ('email_id', models.CharField(default='', max_length=100)),
                ('referrer_code', models.CharField(max_length=30)),
                ('ticket_sales_id', models.IntegerField(default=0)),
                ('payable_amount', models.IntegerField(default=0)),
                ('payment_status', models.CharField(default='', max_length=20)),
                ('payment_date', models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 25, 57, 936617))),
            ],
            options={
                'db_table': 'referrer_cashback_info',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ReferrerCashbackTokens',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('token_code', models.CharField(max_length=100)),
                ('attendee_email_id', models.CharField(default='', max_length=100)),
                ('ticket_sales_id', models.IntegerField()),
            ],
            options={
                'db_table': 'referrer_cashback_tokens',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ShortUrlTracker',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('event_id', models.IntegerField()),
                ('short_url', models.CharField(max_length=20)),
                ('original_url', models.CharField(max_length=150)),
            ],
            options={
                'db_table': 'short_url_tracker',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='StallAudience',
            fields=[
                ('table_id', models.AutoField(primary_key=True, serialize=False)),
                ('audience_type', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'stall_audience',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='StallsAudience',
            fields=[
                ('table_id', models.AutoField(primary_key=True, serialize=False)),
                ('event_id', models.IntegerField()),
                ('stall_id', models.IntegerField()),
                ('audience', models.IntegerField()),
            ],
            options={
                'db_table': 'stalls_audience',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='StallsProducts',
            fields=[
                ('table_id', models.AutoField(primary_key=True, serialize=False)),
                ('event_id', models.IntegerField()),
                ('stall_id', models.IntegerField()),
                ('product_id', models.IntegerField()),
            ],
            options={
                'db_table': 'stalls_products',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Stall',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stall_name', models.CharField(max_length=254)),
                ('total_quantity', models.IntegerField()),
                ('booking_start_date', models.DateField(null=True)),
                ('booking_start_time', models.TimeField(null=True)),
                ('booking_end_date', models.DateField(null=True)),
                ('booking_end_time', models.TimeField(null=True)),
                ('full_stall_price', models.DecimalField(decimal_places=3, max_digits=20)),
                ('half_stall_price', models.DecimalField(decimal_places=3, max_digits=20)),
                ('number_of_chairs', models.IntegerField()),
                ('counter', models.BooleanField(default=True)),
                ('any_condition', models.TextField(max_length=300)),
                ('size_in_meters', models.CharField(max_length=255)),
                ('event_id', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='StallFacilities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power_backup', models.BooleanField()),
                ('parking_space', models.BooleanField()),
                ('water_supply', models.BooleanField()),
                ('electricity', models.BooleanField()),
                ('wifi', models.BooleanField()),
                ('security', models.BooleanField()),
                ('description', models.TextField()),
                ('terms_and_condition', models.TextField()),
                ('expected_footfall', models.IntegerField()),
                ('event_id', models.IntegerField()),
                ('accepted_product', models.ManyToManyField(null=True, related_name='products', to='dashboard.CategoriesStalls')),
                ('type_of_audience', models.ManyToManyField(null=True, related_name='audiences', to='dashboard.StallAudience')),
            ],
        ),
        migrations.CreateModel(
            name='StallUpload',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(upload_to='')),
                ('file', models.FileField(upload_to='')),
                ('event_id', models.IntegerField()),
            ],
        ),
        migrations.RenameField(
            model_name='statusonchannel',
            old_name='active_state',
            new_name='admin_id',
        ),
        migrations.RenameField(
            model_name='statusonchannel',
            old_name='payment_within_days',
            new_name='event_id',
        ),
        migrations.RenameField(
            model_name='statusonchannel',
            old_name='site_url',
            new_name='link',
        ),
        migrations.RenameField(
            model_name='statusonchannel',
            old_name='app',
            new_name='partner_status',
        ),
        migrations.RenameField(
            model_name='statusonchannel',
            old_name='cc',
            new_name='promotion_status',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='additional_msg',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='convenience_fee_organizer',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='country',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='coverage',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='doc_name',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='email1',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='email2',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='flat_fee_organizer',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='merchant_name',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='method',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='negotiated_convenience_fee',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='negotiated_flat_charges',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='negotiated_tax_charges',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='negotiated_transaction_fee',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='official_convenience_fee',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='official_flat_charges',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='official_tax_charges',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='official_transaction_fee',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='payment_policy',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='restriction',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='site_name',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='support_email',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='support_mobile',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='support_name',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='tax_fee_organizer',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='transaction_fee_organizer',
        ),
        migrations.RemoveField(
            model_name='statusonchannel',
            name='website',
        ),
        migrations.AddField(
            model_name='statusonchannel',
            name='last_updated',
            field=models.DateTimeField(default=datetime.datetime(2019, 12, 18, 14, 27, 1, 128450, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AlterModelTable(
            name='statusonchannel',
            table=None,
        ),
    ]
