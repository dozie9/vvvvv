from django.contrib import admin

# Register your models here.
from dashboard.models import CategoriesStalls, StallAudience, Stall, StallFacilities, StallUpload, StallsAudience, \
    StallsProducts

admin.site.register(CategoriesStalls)
admin.site.register(StallsAudience)
admin.site.register(StallsProducts)
admin.site.register(Stall)
admin.site.register(StallFacilities)
admin.site.register(StallUpload)